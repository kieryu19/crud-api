const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');

const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended:false }))

app.use(bodyParser.json())

// mySQL
const pool = mysql.createPool({
	connectionLimit : 10,
	host						: 'localhost',
	user            : 'root',
	password        : '',
	database        : 'users_db',
});


// Get all Users
app.get('/get_users', (req,res) => {
	
	pool.getConnection((err,connection) => {
		if(err) throw err
			console.log(`connected as id ${connection.threadId}`)

			connection.query('SELECT * FROM tbl_users', (err, rows)  => {
				connection.release()

				if(!err) {
					res.send(rows)
				} else {
					console.log(err)
				}

			});

	});

});

// Get Users by id
app.get('/get_users/:id', (req,res) => {
	
	pool.getConnection((err,connection) => {
		if(err) throw err
			console.log(`connected as id ${connection.threadId}`)

			connection.query('SELECT * FROM tbl_users WHERE users_id = ?', [req.params.id], (err, rows)  => {
				connection.release()

				if(!err) {
					res.send(rows)
				} else {
					console.log(err)
				}

			});

	});

});

// Delete a Record
app.delete('/delete_user/:id', (req,res) => {
	
	pool.getConnection((err,connection) => {
		if(err) throw err
			console.log(`connected as id ${connection.threadId}`)

			connection.query('DELETE FROM tbl_users WHERE users_id = ?', [req.params.id], (err, rows)  => {
				connection.release()

				if(!err) {
					res.send('User has been deleted.')
				} else {
					console.log(err)
				}

			});

	});

});

// Add a Record
app.post('/add_users', (req,res) => {
	
	pool.getConnection((err,connection) => {
		if(err) throw err
			console.log(`connected as id ${connection.threadId}`)

			const params = req.body

			connection.query('INSERT INTO tbl_users SET ?', params, (err, rows)  => {
				connection.release()

				if(!err) {
					res.send(`The user ${params.fname} has been added.`)
				} else {
					console.log(err)
				}

			});

			console.log(req.body);

	});

});

// Update a Record
app.put('/update_users', (req,res) => {
	
	pool.getConnection((err,connection) => {
		if(err) throw err
			console.log(`connected as id ${connection.threadId}`)

			const params = req.body
			const {users_id, fname, lname, address, postcode, contact_no, email, username, password} = req.body

			connection.query('UPDATE tbl_users SET fname = ?,lname = ?, address = ?, postcode = ?, contact_no = ?, email = ?, username = ?, password = ? WHERE users_id = ?', [fname,lname,address,postcode,contact_no,email,username,password,users_id], (err, rows)  => {
				connection.release()

				if(!err) {
					res.send(`Record of ${params.fname} has been updated.`)
				} else {
					console.log(err)
				}

			});

			console.log(req.body);

	});

});

// Delete Multiple Record
app.delete('/delete_multiple_user', (req,res) => {

	
	pool.getConnection((err,connection) => {
		if(err) throw err
			console.log(`connected as id ${connection.threadId}`)

			const params = req.body

			debugger

			connection.query('DELETE FROM tbl_users WHERE users_id IN (?)', params, (err, rows)  => {
			connection.release()

				if(!err) {
					res.send('Multiple Users has been deleted.')
				} else {
					console.log(err)
				}

			});

	});

});


// Listen on 3000
app.listen(port, () => console.log(`Listen on port ${port}`));